# skipthedishes

## General ##

Implementing an stub api in java8 using spring-boot with the following requirements:

* Allow Authentication;
* Query Products;
* Receive Orders;
* Cancel an Order;
* Get Order Status;
* Store data in a database of his/her choice;

## Database ##

Used mysql as the database.
* The standard properties are  dbname:skip, user:skip, password:skip
* Schema is automatically generated using liquibase

## Customizing ##

Several aspects can be modified at the application.properties file


## Authentication ##

The authentication relies on JWT.

First the user must login to */api/v1/customer/auth*. The endpoint shall return a jwt token that should be used upon any subsequent calls

Any call except the ones for the */customer* endpoint requires the user to pass an authentication token at the *Authorization* header:

Follows one example

Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsdWl6aGVucmlxdWUucm9saW1AZ21haWwuY29tIiwiZXhwIjoxNTIyMDEzNTQ1LCJpYXQiOjE1MjE0MDg3NDV9.vIXmwCpkjx9zgoAa9L8NiHh1qq407A3wl14BnhXegBGza4LwqXVA9tlLf3JtqiDqIz6RTJCyDsytteyUt3qw3A

## Extra Caveats ##

Some tables were populated directly at the database since the api wasn´t really complete.

Such tables were:
*Cousine
*Store
*Product

## TODOS ##

* Create unit tests
* Implement async processing using RabbitMQ/Redis or other Messaging technologies
* Cache results using Redis/MemCache
* Split towards microservice architecture (ex: Product, Store , Customer apps)
* Improve error handling
* Document the API

## Further Considerations ##

Of course the scope of the above analysis is limited to a 8h hackaton experience. 
There are several other things that could be improved, in terms of scalability, searching capabilities, database design. 
However I think these would require a deeper analysis.

