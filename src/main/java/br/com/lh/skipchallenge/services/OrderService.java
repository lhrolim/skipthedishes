package br.com.lh.skipchallenge.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.lh.skipchallenge.controller.dto.request.OrderRequestDto;
import br.com.lh.skipchallenge.model.entities.Order;
import br.com.lh.skipchallenge.model.entities.OrderItem;
import br.com.lh.skipchallenge.model.entities.OrderStatus;
import br.com.lh.skipchallenge.model.entities.Store;
import br.com.lh.skipchallenge.model.repository.OrderRepository;
import br.com.lh.skipchallenge.model.repository.StoreRepository;

@Service
public class OrderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

	@Autowired OrderRepository orderRepository;
	
	@Autowired StoreRepository storeRepository;

	@Transactional(noRollbackFor = Throwable.class)
	public void save(OrderRequestDto request, Long customerId) {

		Optional<Store> store = storeRepository.findById(request.getStoreId());
		if (!store.isPresent()) {
			throw new IllegalStateException("order cannot be placed. Unexistent store: " + request.getStoreId());
		}
		
		//TODO: make it async
		Order order = new Order();
		order.setContact(request.getContact());
		order.setCustomer(customerId);
		Date now = Calendar.getInstance().getTime();
		order.setDate(now);
		order.setStatus(OrderStatus.PENDING);
		order.setDeliveryAddress(request.getDeliveryAddress());
		order.setLastupdate(now);
		order.setStore(store.get());
		order.setTotal(request.getTotal());
		
		Collection<OrderItem> items = new ArrayList<>();
		
		request.getOrderItems().forEach(i -> items.add(i.convert()));
		order.setItems(items);
		
		Order persistedOrder = orderRepository.save(order);

		LOGGER.info("Finish method save for order {}", persistedOrder);
	}

	public void cancel(Long orderId) {
		Optional<Order> orderOpt = orderRepository.findById(orderId);
		if (!orderOpt.isPresent()) {
			throw new IllegalStateException("order " + orderId + " not found");
		}
		Order order = orderOpt.get();
		order.setStatus(OrderStatus.CANCELED);
		orderRepository.save(order);
	}

	

}
