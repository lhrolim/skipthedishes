package br.com.lh.skipchallenge.services;

import java.util.Calendar;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lh.skipchallenge.controller.dto.request.CustomerAuthRequestDto;
import br.com.lh.skipchallenge.controller.dto.request.CustomerRequestDto;
import br.com.lh.skipchallenge.model.entities.Customer;
import br.com.lh.skipchallenge.model.repository.CustomerRepository;
import br.com.lh.skipchallenge.util.JwtTokenUtil;
import br.com.lh.skipchallenge.util.PasswordUtil;

@Service
public class CustomerService {
	
	@Autowired
	public CustomerRepository customerRepo;
	
	@Autowired
	public JwtTokenUtil jwtTokenUtil;
	
	@Transactional
	public void create(CustomerRequestDto customerDto) {
		
		Customer customer = new Customer();
		
		customer.setAddress(customerDto.getAddress());
		customer.setEmail(customerDto.getEmail());
		customer.setName(customerDto.getName());
		customer.setCreation(Calendar.getInstance().getTime());
		//TODO: add password rules for validation
		customer.setPassword(customerDto.getPassword());
		
		customerRepo.save(customer);
	}
	
	@Transactional
	public String auth(CustomerAuthRequestDto reqDto) {
		
		Customer customer = customerRepo.findByEmail(reqDto.getEmail());
		if (customer == null || !PasswordUtil.matches(reqDto.getPassword(), customer.getPassword())) {
			//user not found shouldn�t return a token
			return null;
		}
		return jwtTokenUtil.generateToken(reqDto.getEmail());
	
	}
	
	
	
	
}
