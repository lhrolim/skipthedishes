package br.com.lh.skipchallenge.controller.dto.response.store;

import java.util.Collection;

import br.com.lh.skipchallenge.controller.dto.response.SuccessResponseDto;

public class StoreListResponse extends SuccessResponseDto {

	private Collection<StoreResponseDto> cousines;

	public StoreListResponse(Collection<StoreResponseDto> cousineList) {
		this.cousines = cousineList;
	}

	public Collection<StoreResponseDto> getCousines() {
		return cousines;
	}

	public void setCousines(Collection<StoreResponseDto> cousines) {
		this.cousines = cousines;
	}

}
