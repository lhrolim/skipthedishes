package br.com.lh.skipchallenge.controller.dto.request;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class CustomerRequestDto implements Serializable {

	private static final long serialVersionUID = -1667909425363718813L;

	@ApiModelProperty(required = true, example = "xxx@yyy.com", position = 1)
	@NotBlank(message = "Email cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String email;

	@ApiModelProperty(required = true, example = "Luiz Rolim", position = 2)
	@NotBlank(message = "Name cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String name;

	@ApiModelProperty(required = false, example = "Francisco Morato Av 4886 apto 286", position = 3)
	@NotBlank(message = "Address Cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String address;

	@ApiModelProperty(required = false, example = "mypassword", position = 4)
	@NotBlank(message = "Password cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public interface SaveGroup {
	}

}
