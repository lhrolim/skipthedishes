package br.com.lh.skipchallenge.controller.dto.response.order;

import java.math.BigDecimal;

import br.com.lh.skipchallenge.model.entities.OrderItem;

public class OrderItemDto {

	private String productName;
	private BigDecimal price;
	private Integer quantity;
	private BigDecimal total;

	public OrderItemDto(OrderItem item) {
		super();
		this.productName = item.getProduct().getName();
		this.price = item.getPrice();
		this.quantity = item.getQuantity();
		this.total = item.getTotal();
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

}
