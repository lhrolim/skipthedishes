package br.com.lh.skipchallenge.controller.dto.response.order;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import br.com.lh.skipchallenge.controller.dto.response.SuccessResponseDto;
import br.com.lh.skipchallenge.model.entities.Order;
import br.com.lh.skipchallenge.model.entities.OrderStatus;

public class OrderResponseDto extends SuccessResponseDto {

	public OrderResponseDto(Order order, boolean includeItemDetails) {
		this.storeName = order.getStore().getName();
		this.date = order.getDate();
		this.total = order.getTotal();
		this.status = order.getStatus();

		if (includeItemDetails) {
			this.items = new ArrayList<>();
			order.getItems().forEach(i -> this.items.add(new OrderItemDto(i)));
		}
	}

	private String storeName;

	private Date date;

	private Collection<OrderItemDto> items;

	private BigDecimal total;

	private OrderStatus status;

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Collection<OrderItemDto> getItems() {
		return items;
	}

	public void setItems(Collection<OrderItemDto> items) {
		this.items = items;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

}
