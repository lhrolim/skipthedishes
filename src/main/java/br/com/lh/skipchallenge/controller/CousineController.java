package br.com.lh.skipchallenge.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.lh.skipchallenge.controller.dto.request.CousineRequestDto;
import br.com.lh.skipchallenge.controller.dto.response.BaseResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.CousineDto;
import br.com.lh.skipchallenge.controller.dto.response.CousineListResponse;
import br.com.lh.skipchallenge.model.entities.Cousine;
import br.com.lh.skipchallenge.model.entities.Store;
import br.com.lh.skipchallenge.model.repository.CousineRepository;
import br.com.lh.skipchallenge.model.repository.StoreRepository;

//import com.vanhack.dishes.model.ResponseDetail;
//import com.vanhack.dishes.model.ResponseStatus;
//import com.vanhack.dishes.model.request.CustomerRequest;
//import com.vanhack.dishes.model.response.CustomerResponse;
//import com.vanhack.dishes.service.CustomerService;
//import com.vanhack.dishes.utils.Locales;

import io.swagger.annotations.Api;

@RestController
@Api(tags = { "Customer" })

public class CousineController extends ARestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CousineController.class);

	@Autowired
	private CousineRepository cousineRepository;

	@Autowired
	private StoreRepository storeRepository;

	@GetMapping(produces = JSON, value= "/api/v1/Cousine")
	public ResponseEntity<? extends BaseResponseDto> getCousines() {
		LOGGER.info("list cousines api call");
		Iterable<Cousine> cousines = cousineRepository.findAll();
		Collection<CousineDto> cousineList = new ArrayList<>();
		cousines.forEach(c -> cousineList.add(new CousineDto(c)));
		CousineListResponse response = new CousineListResponse(cousineList);
		return new ResponseEntity<CousineListResponse>(response, HttpStatus.OK);
	}

	@GetMapping("/api/v1/Cousine/{cousineId}/stores")
	@ResponseBody
	public ResponseEntity<Collection<Store>> getRestaurantByCousineID(@PathVariable("cousineId") Integer cousineId) {
		if (cousineId == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}

		Optional<Cousine> cousine = cousineRepository.findById(cousineId);
		if (!cousine.isPresent()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ArrayList<>());
		}

		Collection<Store> store = storeRepository.findByCousine(cousine.get());
		if (store.isEmpty()) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ArrayList<>());
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(store);
		}

	}

	@GetMapping("/api/v1/Cousine/search/{searchText}")
	@ResponseBody
	public ResponseEntity<Collection<Cousine>> getCousineContainName(@PathVariable("searchText") String searchText) {

		Collection<Cousine> cousines = cousineRepository.findByNameContaining(searchText);
		return ResponseEntity.status(HttpStatus.OK).body(cousines);
	}

	@PostMapping("/api/v1/Cousine/add")
	@ResponseBody
	public ResponseEntity<String> cousineAdd(
			@RequestBody @Validated({ CousineRequestDto.SaveGroup.class }) CousineRequestDto request) {

		Optional<Cousine> optCousine = cousineRepository.findByName(request.getName());
		Cousine cousine = new Cousine();
		if (optCousine.isPresent()) {
			// forcing an update
			cousine.setId(optCousine.get().getId());
		}

		cousine.setName(request.getName());
		cousine.setDescription(request.getDescription());
		cousine.setName(request.getName());
		cousineRepository.save(cousine);
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

}
