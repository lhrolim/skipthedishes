package br.com.lh.skipchallenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import br.com.lh.skipchallenge.controller.dto.response.ErrorResponseDto;

@Component
public class ARestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ARestController.class);
	
	protected static final String JSON = MediaType.APPLICATION_JSON_UTF8_VALUE;
	

//	
	public ResponseEntity<ErrorResponseDto> internalError(Exception exception) {
		LOGGER.error(exception.getMessage(), exception);
		ErrorResponseDto response = new ErrorResponseDto(exception.getMessage());
		LOGGER.info("Response defaultError: {}", response);
		return new ResponseEntity<ErrorResponseDto>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	

}
