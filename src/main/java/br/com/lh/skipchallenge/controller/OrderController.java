package br.com.lh.skipchallenge.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.lh.skipchallenge.controller.dto.request.OrderRequestDto;
import br.com.lh.skipchallenge.controller.dto.response.BaseResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.SuccessResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.order.OrderListResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.order.OrderResponseDto;
import br.com.lh.skipchallenge.model.entities.Order;
import br.com.lh.skipchallenge.model.entities.OrderStatus;
import br.com.lh.skipchallenge.model.repository.OrderRepository;
import br.com.lh.skipchallenge.security.JwtUser;
import br.com.lh.skipchallenge.services.OrderService;

//import com.vanhack.dishes.model.ResponseDetail;
//import com.vanhack.dishes.model.ResponseStatus;
//import com.vanhack.dishes.model.request.CustomerRequest;
//import com.vanhack.dishes.model.response.CustomerResponse;
//import com.vanhack.dishes.service.CustomerService;
//import com.vanhack.dishes.utils.Locales;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = { "Order" })
public class OrderController extends ARestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private OrderService orderService;

	@GetMapping("/api/v1/Order/{orderId}")
	@ResponseBody
	@ApiOperation(value = "Load Order")
	public ResponseEntity<OrderResponseDto> getOrderDetail(@PathVariable("orderId") Long orderId) {
		LOGGER.info("get order detail api called");
		if (orderId == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		Optional<Order> optproduct = orderRepository.findById(orderId);
		if (!optproduct.isPresent()) {
			LOGGER.warn("order {} not found", orderId);
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		Order order = optproduct.get();
		OrderResponseDto orderResponse = new OrderResponseDto(order,true);
		return new ResponseEntity<OrderResponseDto>(orderResponse,HttpStatus.OK);
	}
	
	@GetMapping("/api/v1/Order/{orderId}/status")
	@ResponseBody
	@ApiOperation(value = "Order Status")
	public ResponseEntity<OrderStatus> getOrderStatus(@PathVariable("orderId") Long orderId) {
		if (orderId == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		Optional<Order> optproduct = orderRepository.findById(orderId);
		if (!optproduct.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		Order order = optproduct.get();
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(order.getStatus());
	}
	
	@GetMapping("/api/v1/Order/customer")
	@ResponseBody
	@ApiOperation(value = "Orders of Customer")
	public ResponseEntity<OrderListResponseDto> getOrdersByCustomer() {
		
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		JwtUser user = (JwtUser)authentication.getDetails();
		
		Collection<Order> orders = orderRepository.findByCustomer(user.getId());
		Collection<OrderResponseDto> ordersdtoList = new ArrayList<>();
		orders.forEach(o -> ordersdtoList.add(new OrderResponseDto(o, false)));
		OrderListResponseDto orderResponse = new OrderListResponseDto(ordersdtoList);
		return new ResponseEntity<OrderListResponseDto>(orderResponse,HttpStatus.OK);
	}
	
	@ApiOperation(value = "Place Order")
	@org.springframework.web.bind.annotation.ResponseStatus(value = HttpStatus.CREATED)
	@PostMapping(produces = JSON, consumes = JSON, value= "/api/v1/Order")
	public ResponseEntity<? extends BaseResponseDto> place(
			@RequestBody @Validated({OrderRequestDto.SaveGroup.class}) OrderRequestDto request) throws Exception {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			JwtUser user = (JwtUser)authentication.getDetails();
			orderService.save(request,user.getId());
			return new ResponseEntity<BaseResponseDto>(new SuccessResponseDto(), HttpStatus.CREATED);
		}catch (Exception e) {
			return internalError(e);
		}
	}
	
	
	@ApiOperation(value = "Cancel Order")
	@org.springframework.web.bind.annotation.ResponseStatus(value = HttpStatus.OK)
	@PutMapping(value = "/api/v1/Order/{orderId}/cancel", produces = JSON, consumes = JSON)
	public ResponseEntity<? extends BaseResponseDto> cancel(
			@NotNull(message = "Order Id cannot be empty") @PathVariable(value = "orderId") Long orderId){
		if (orderId==null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		
		try {
			orderService.cancel(orderId);
			return new ResponseEntity<BaseResponseDto>(new SuccessResponseDto(), HttpStatus.OK);
		}catch (Exception e) {
			return internalError(e);
		}
	}

}
