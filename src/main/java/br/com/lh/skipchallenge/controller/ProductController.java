package br.com.lh.skipchallenge.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.lh.skipchallenge.controller.dto.response.BaseResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.product.ProductListResponse;
import br.com.lh.skipchallenge.controller.dto.response.product.ProductResponse;
import br.com.lh.skipchallenge.model.entities.Product;
import br.com.lh.skipchallenge.model.repository.ProductRepository;

//import com.vanhack.dishes.model.ResponseDetail;
//import com.vanhack.dishes.model.ResponseStatus;
//import com.vanhack.dishes.model.request.CustomerRequest;
//import com.vanhack.dishes.model.response.CustomerResponse;
//import com.vanhack.dishes.service.CustomerService;
//import com.vanhack.dishes.utils.Locales;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = { "Product" })
public class ProductController extends ARestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductRepository productRepository;

	@GetMapping(produces = JSON, value="/api/v1/Product")
	@ApiOperation(value = "Load All Products")
	public ResponseEntity<? extends BaseResponseDto> getProducts() {
		LOGGER.info("querying products api called");
		Iterable<Product> products = productRepository.findAll();
		ProductListResponse response = convertToDto(products);
		return new ResponseEntity<ProductListResponse>(response, HttpStatus.OK);
	}

	private ProductListResponse convertToDto(Iterable<Product> products) {
		Collection<ProductResponse> productList = new ArrayList<>();
		products.forEach(p -> productList.add(new ProductResponse(p)));
		ProductListResponse response = new ProductListResponse(productList);
		return response;
	}

	@GetMapping("/api/v1/Product/search/{searchText}")
	@ResponseBody
	@ApiOperation(value = "Search Product")
	public ResponseEntity<? extends BaseResponseDto> getProductByName(
			@PathVariable("searchText") String searchText) {
		LOGGER.info("querying products by name api called");
		Collection<Product> products = productRepository.findByNameContaining(searchText);
		ProductListResponse response = convertToDto(products);
		return new ResponseEntity<ProductListResponse>(response, HttpStatus.OK);
	}

	@GetMapping("/api/v1/Product/{productId}")
	@ResponseBody
	@ApiOperation(value = "Load Product Detail")
	public ResponseEntity<Product> getProductByID(@PathVariable("productId") Long productId) {
		LOGGER.info("Load product id");
		if (productId == null) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
		Optional<Product> optproduct = productRepository.findById(productId);
		if (!optproduct.isPresent()) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(optproduct.get());
		}
	}

}
