package br.com.lh.skipchallenge.controller.dto.request;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class CousineRequestDto implements Serializable {

	private static final long serialVersionUID = -1667909425363718813L;


	@ApiModelProperty(required = true, example = "Italian", position = 1)
	@NotBlank(message = "Name cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String name;
	
	@ApiModelProperty(required = true, example = "mamma mia! ", position = 2)
	@JsonProperty
	private String description;



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}



	public interface SaveGroup {
	}


}
