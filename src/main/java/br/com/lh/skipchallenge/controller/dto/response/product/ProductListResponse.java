package br.com.lh.skipchallenge.controller.dto.response.product;

import java.util.Collection;

import br.com.lh.skipchallenge.controller.dto.response.SuccessResponseDto;

public class ProductListResponse extends SuccessResponseDto {

	private Collection<ProductResponse> products;

	public ProductListResponse(Collection<ProductResponse> products) {
		super();
		this.products = products;
	}

	public Collection<ProductResponse> getProducts() {
		return products;
	}

	public void setProducts(Collection<ProductResponse> products) {
		this.products = products;
	}

}
