package br.com.lh.skipchallenge.controller;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.lh.skipchallenge.controller.dto.response.BaseResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.store.StoreListResponse;
import br.com.lh.skipchallenge.controller.dto.response.store.StoreResponseDto;
import br.com.lh.skipchallenge.model.entities.Store;
import br.com.lh.skipchallenge.model.repository.StoreRepository;

//import com.vanhack.dishes.model.ResponseDetail;
//import com.vanhack.dishes.model.ResponseStatus;
//import com.vanhack.dishes.model.request.CustomerRequest;
//import com.vanhack.dishes.model.response.CustomerResponse;
//import com.vanhack.dishes.service.CustomerService;
//import com.vanhack.dishes.utils.Locales;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = { "Store" })

public class StoreController extends ARestController {

	private static final Logger LOGGER = LoggerFactory.getLogger(StoreController.class);


	@Autowired
	private StoreRepository storeRepository;

	@GetMapping(produces = JSON,value="/api/v1/Store")
	@ApiOperation(value = "Load Stores")
	public ResponseEntity<? extends BaseResponseDto> getStores() {
		LOGGER.info("load all stores called");
		Iterable<Store> stores = storeRepository.findAll();
		Collection<StoreResponseDto> storeList = new ArrayList<>();
		stores.forEach(s -> storeList.add(new StoreResponseDto(s)));
		StoreListResponse response = new StoreListResponse(storeList);
		return new ResponseEntity<StoreListResponse>(response, HttpStatus.OK);
	}

	
	@GetMapping("/api/v1/Store/search/{searchText}")
	@ResponseBody
	@ApiOperation(value = "Search Store")
	public ResponseEntity<Collection<Store>> serchStore(@PathVariable("searchText") String searchText) {
		LOGGER.info("searching store by {}",searchText);
		Collection<Store> stores = storeRepository.findByNameContaining(searchText);
		return ResponseEntity.status(HttpStatus.OK).body(stores);
	}

	

}
