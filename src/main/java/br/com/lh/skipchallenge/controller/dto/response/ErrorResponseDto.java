package br.com.lh.skipchallenge.controller.dto.response;

public class ErrorResponseDto extends BaseResponseDto {

	public ErrorResponseDto(String message) {
		super(500, message);
	}

}
