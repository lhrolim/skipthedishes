package br.com.lh.skipchallenge.controller.dto.response;

import java.util.Collection;

public class CousineListResponse extends BaseResponseDto {

	private Collection<CousineDto> cousines;

	public CousineListResponse(Collection<CousineDto> cousineList) {
		this.cousines = cousineList;
	}

	public Collection<CousineDto> getCousines() {
		return cousines;
	}

	public void setCousines(Collection<CousineDto> cousines) {
		this.cousines = cousines;
	}

}
