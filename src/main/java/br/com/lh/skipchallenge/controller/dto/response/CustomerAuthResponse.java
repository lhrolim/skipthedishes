package br.com.lh.skipchallenge.controller.dto.response;

public class CustomerAuthResponse extends SuccessResponseDto {

	private String token;

	public CustomerAuthResponse(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
