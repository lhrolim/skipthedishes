package br.com.lh.skipchallenge.controller.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@JsonInclude(Include.NON_NULL)
public class BaseResponseDto {

	@ApiModelProperty(required = true, position = 0)
	@JsonProperty("status")
	private int responseCode;

	@ApiModelProperty(position = 1)
	@JsonProperty("message")
	private String message;
	
	

	public BaseResponseDto(int responseCode, String message) {
		super();
		this.responseCode = responseCode;
		this.message = message;
	}
	
	
	public BaseResponseDto() {
		super();
	}

	public int getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
