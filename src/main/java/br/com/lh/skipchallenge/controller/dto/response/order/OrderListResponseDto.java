package br.com.lh.skipchallenge.controller.dto.response.order;

import java.util.Collection;

import br.com.lh.skipchallenge.controller.dto.response.SuccessResponseDto;

public class OrderListResponseDto extends SuccessResponseDto {

	private Collection<OrderResponseDto> orderResponseDto;

	public OrderListResponseDto(Collection<OrderResponseDto> orderResponseDto) {
		super();
		this.orderResponseDto = orderResponseDto;
	}

	public Collection<OrderResponseDto> getOrderResponseDto() {
		return orderResponseDto;
	}

	public void setOrderResponseDto(Collection<OrderResponseDto> orderResponseDto) {
		this.orderResponseDto = orderResponseDto;
	}
	
	

}
