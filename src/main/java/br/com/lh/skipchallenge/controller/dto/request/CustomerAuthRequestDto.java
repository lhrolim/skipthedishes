package br.com.lh.skipchallenge.controller.dto.request;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class CustomerAuthRequestDto implements Serializable {

	private static final long serialVersionUID = -1667909425363718813L;

	@ApiModelProperty(required = true, example = "xxx@yyy.com", position = 1)
	@NotBlank(message = "Email cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String email;

	@ApiModelProperty(required = false, example = "mypassword", position = 2)
	@NotBlank(message = "Password cannot be blank", groups = { SaveGroup.class })
	@JsonProperty
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public interface SaveGroup {
	}

}
