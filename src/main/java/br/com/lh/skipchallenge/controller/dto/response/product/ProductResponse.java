package br.com.lh.skipchallenge.controller.dto.response.product;

import java.math.BigDecimal;

import br.com.lh.skipchallenge.controller.dto.response.BaseResponseDto;
import br.com.lh.skipchallenge.model.entities.Product;

public class ProductResponse extends BaseResponseDto implements Comparable<ProductResponse> {

	private String storeName;

	private String name;

	private String description;

	private BigDecimal price;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public ProductResponse() {
		super();
	}

	public ProductResponse(Product p) {
		this.id = p.getId();
		this.name = p.getName();
		this.description = p.getDescription();
		this.price = p.getPrice();
		this.storeName = p.getStore().getName();
	}

	@Override
	public int compareTo(ProductResponse o) {
		return getPrice().compareTo(o.getPrice());
	}

}
