package br.com.lh.skipchallenge.controller.dto.response;

import br.com.lh.skipchallenge.model.entities.Cousine;

public class CousineDto extends BaseResponseDto {

	private int id;

	private String name;

	private String description;

	public CousineDto(Cousine c) {
		this.id = c.getId();
		this.name = c.getName();
		this.description = c.getDescription();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
