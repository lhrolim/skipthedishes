package br.com.lh.skipchallenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.lh.skipchallenge.controller.dto.request.CustomerAuthRequestDto;
import br.com.lh.skipchallenge.controller.dto.request.CustomerRequestDto;
import br.com.lh.skipchallenge.controller.dto.response.BaseResponseDto;
import br.com.lh.skipchallenge.controller.dto.response.CustomerAuthResponse;
import br.com.lh.skipchallenge.services.CustomerService;

//import com.vanhack.dishes.model.ResponseDetail;
//import com.vanhack.dishes.model.ResponseStatus;
//import com.vanhack.dishes.model.request.CustomerRequest;
//import com.vanhack.dishes.model.response.CustomerResponse;
//import com.vanhack.dishes.service.CustomerService;
//import com.vanhack.dishes.utils.Locales;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(tags = { "Customer" })

public class CustomerController extends ARestController{

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

	@Autowired
	private CustomerService customerService;

	

	@ApiOperation(value = "Create Customer")
	@PostMapping(produces = JSON, consumes = JSON, value="/api/v1/customer")
	public ResponseEntity<? extends BaseResponseDto> create(
			@RequestBody @Validated({ CustomerRequestDto.SaveGroup.class }) CustomerRequestDto request) {

		LOGGER.info("Creating Customer Request: {}", request.toString());
		try {
			customerService.create(request);
			return new ResponseEntity<BaseResponseDto>(HttpStatus.OK);
		} catch (Exception e) {
			return internalError(e);
		}
	}
	
	@ApiOperation(value = "Auth Customer")
	@PostMapping(value="/api/v1/customer/auth",produces = JSON, consumes = JSON)
	public ResponseEntity<? extends BaseResponseDto> auth(@RequestBody CustomerAuthRequestDto request) {

		LOGGER.info("Authing customer Request: {}", request.toString());
		try {
			String token = customerService.auth(request);
			CustomerAuthResponse response = new CustomerAuthResponse(token);
			return new ResponseEntity<CustomerAuthResponse>(response, HttpStatus.OK);
		} catch (Exception e) {
			return internalError(e);
		}
	}
	
}
