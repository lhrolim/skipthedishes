package br.com.lh.skipchallenge.controller.dto.request;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

public class OrderRequestDto implements Serializable {

	private static final long serialVersionUID = -1667909425363718813L;

	@NotNull(message = "Delivery Address Cannot be empty", groups = { SaveGroup.class })
	@ApiModelProperty(required = true, example = "2991 Pembina Hwy, Winnipeg, Manitoba R3T 2H5, Canada", position = 2)
	@JsonProperty
	private String deliveryAddress;

	@NotNull(message = "Contact cannot be empty", groups = { SaveGroup.class })
	@ApiModelProperty(required = true, example = "Doug", position = 3)
	@JsonProperty
	private String contact;

	@NotNull(message = "Store Id cannot be empty", groups = { SaveGroup.class })
	@ApiModelProperty(required = true, example = "1", position = 4)
	@JsonProperty
	private Long StoreId;

	@ApiModelProperty(required = false, position = 5)
	@Valid
	@JsonProperty
	private List<OrderItemRequest> orderItems;

	@NotNull(message = "invalid.order.total", groups = { SaveGroup.class })
	@DecimalMin(value = "0.01", message = "order total cannot be null")
	@ApiModelProperty(required = false, example = "1.43", position = 6)
	@JsonProperty
	private BigDecimal total;


	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Long getStoreId() {
		return StoreId;
	}

	public void setStoreId(Long storeId) {
		StoreId = storeId;
	}

	public List<OrderItemRequest> getOrderItems() {
		return orderItems;
	}

	public void setOrderItems(List<OrderItemRequest> orderItems) {
		this.orderItems = orderItems;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public interface SaveGroup {
	}

}
