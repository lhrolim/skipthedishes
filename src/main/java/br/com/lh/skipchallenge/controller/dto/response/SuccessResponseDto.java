package br.com.lh.skipchallenge.controller.dto.response;

public class SuccessResponseDto extends BaseResponseDto {

	public SuccessResponseDto() {
		super(200,null);
	}

}
