package br.com.lh.skipchallenge.controller.dto.response.store;

import br.com.lh.skipchallenge.controller.dto.response.SuccessResponseDto;
import br.com.lh.skipchallenge.model.entities.Store;

public class StoreResponseDto extends SuccessResponseDto {

	private long id;

	private String name;

	private String description;

	public StoreResponseDto(Store s) {
		this.id = s.getId();
		this.name = s.getName();
//		this.description = s.getDescription();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
