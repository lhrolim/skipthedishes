package br.com.lh.skipchallenge.model.repository;

import java.util.Collection;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.lh.skipchallenge.model.entities.Cousine;

public interface CousineRepository extends CrudRepository<Cousine, Integer> {
	Collection<Cousine> findByNameContaining(@Param("name") String name);

	Optional<Cousine> findByName(@Param("name") String name);
}
