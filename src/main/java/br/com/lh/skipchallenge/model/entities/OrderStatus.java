package br.com.lh.skipchallenge.model.entities;

public enum OrderStatus {

	DELIVERED,
	PENDING,
	CANCELED;
	
}
