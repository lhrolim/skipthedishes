package br.com.lh.skipchallenge.model.entities;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sorder")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NamedQueries({
	@NamedQuery(name = "Order.activeByCustomer",
			query = "from Order o where o.customer = ?1 and o.status = Pending")	
})

public class Order extends BaseModel {

	private static final long serialVersionUID = 5109656243165439048L;

	@Column(nullable = false, name="customer_id")
	private Long customer;

	@Column(nullable = false)
	private String deliveryAddress;

	@Column(nullable = false)
	private String contact;

	@Column(nullable = false, name="orderdate")
	private Date date;

	@Column(nullable = false)
	private Date lastupdate;

	@OneToMany(cascade = CascadeType.ALL, targetEntity = OrderItem.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "order_id", nullable = false)
	private Collection<OrderItem> items;

	@Column
	private BigDecimal total;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private OrderStatus status;

	@ManyToOne
	@JoinColumn(name = "store_id", referencedColumnName = "id")
	private Store store;

	public Store getStore() {
		return store;
	}

	public void setStore(Store store) {
		this.store = store;
	}

	public Long getCustomer() {
		return customer;
	}

	public void setCustomer(Long customer) {
		this.customer = customer;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Collection<OrderItem> getItems() {
		return items;
	}

	public void setItems(Collection<OrderItem> items) {
		this.items = items;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public OrderStatus getStatus() {
		return status;
	}

	public void setStatus(OrderStatus status) {
		this.status = status;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getLastupdate() {
		return lastupdate;
	}

	public void setLastupdate(Date lastupdate) {
		this.lastupdate = lastupdate;
	}

	@Override
	public String toString() {
		return "Order [customer=" + customer + ", date=" + date + ", total=" + total + ", status=" + status + ", store="
				+ store + "]";
	}

}
