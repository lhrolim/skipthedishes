package br.com.lh.skipchallenge.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "store")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Store extends BaseModel {

	private static final long serialVersionUID = -441264911631137059L;
	@Column(nullable = false)
	private String name;
	
	//TODO: make it many-to-many
	@ManyToOne
	@JoinColumn(name = "cousine_id", referencedColumnName = "id")
	private Cousine cousine;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Store other = (Store) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Store [name=" + name + "]";
	}

}
