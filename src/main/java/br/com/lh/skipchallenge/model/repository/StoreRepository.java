package br.com.lh.skipchallenge.model.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.lh.skipchallenge.model.entities.Cousine;
import br.com.lh.skipchallenge.model.entities.Store;

public interface StoreRepository extends CrudRepository<Store, Long> {

	Collection<Store> findByCousine(Cousine cousine);
	
	Collection<Store> findByNameContaining(@Param("name") String name);
}
