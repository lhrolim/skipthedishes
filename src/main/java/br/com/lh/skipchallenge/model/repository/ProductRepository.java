package br.com.lh.skipchallenge.model.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.com.lh.skipchallenge.model.entities.Product;

public interface ProductRepository extends CrudRepository<Product, Long>, JpaSpecificationExecutor<Product> {

	
	Collection<Product> findByNameContaining(@Param("name") String name);
}
