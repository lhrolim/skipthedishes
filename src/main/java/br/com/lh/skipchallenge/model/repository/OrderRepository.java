package br.com.lh.skipchallenge.model.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import br.com.lh.skipchallenge.model.entities.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {

	Collection<Order> findByCustomer(long customerId);
	
}
