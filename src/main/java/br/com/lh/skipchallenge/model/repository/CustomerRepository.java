package br.com.lh.skipchallenge.model.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.lh.skipchallenge.model.entities.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Long> {

	public Customer findByEmail(String email);
}
